package application.example.impls;

import application.example.interfaces.MP3Dao;
import application.example.objects.Author;
import application.example.objects.MP3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Component("sqliteDAO")
public class SQLiteDao implements MP3Dao {

    @Autowired
    private SimpleJdbcInsert insertMP3;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplateNamed;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, timeout = 1, isolation = Isolation.SERIALIZABLE)
    public int insertMp3(MP3 mp3) {
//        String sql = "insert into mp3 (name, author) values (?, ?)";
//        jdbcTemplate.update(sql, new Object[] {mp3.getName(), mp3.getAuthor()});

        System.out.println(TransactionSynchronizationManager.isActualTransactionActive());

        int author_id = insertAuthor(mp3.getAuthor());

        String sql = "insert into mp3 (authorId, name) values (:authorId, :mp3name)";

        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("authorId", author_id);
        param.addValue("mp3name", mp3.getName());

        return jdbcTemplateNamed.update(sql, param);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int insertAuthor(Author author){
        System.out.println(TransactionSynchronizationManager.isActualTransactionActive());
        String sql = "insert into author (name) values (:authorName)";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("authorName", author.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplateNamed.update(sql, param, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public int insert2(MP3 mp3){
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("name", mp3.getName());
        param.addValue("author", mp3.getAuthor());
        return insertMP3.execute(param);
    }

//    public void insert(List<MP3> mp3List){
//        for (MP3 row : mp3List) {
//            insert(row);
//        }
//    }

    public void delete(int id) {
        String sql = "delete from mp3 where id=?";
        int result = jdbcTemplate.update(sql, id);
    }

    public MP3 getMP3ById(int id) {
        String sql = "select * from mp3 where id=:id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        return jdbcTemplate.queryForObject(sql, new MP3RowMapper(), params);
    }

    public List<MP3> getMP3ListByName(String name) {
        String sql = "select * from mp3 where upper(name) like :name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", "%"+name.toUpperCase()+"%");
        return jdbcTemplate.query(sql, new MP3RowMapper(), params);
    }

    public List<MP3> getMP3ListByAuthor(String author) {
        String sql = "select * from mp3 where upper(name) like :name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", "%"+author.toUpperCase()+"%");
        return jdbcTemplate.query(sql, new MP3RowMapper(), params);
    }

    @Override
    public Map<String, Integer> getStat() {
        String sql = "select author count(*) as count from mp3 group by author";
        return jdbcTemplate.query(sql, new MP3ResultSetEtractor());
    }


    private static final class MP3ResultSetEtractor implements ResultSetExtractor<Map<String, Integer>> {

        @Override
        public Map<String, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<String, Integer> map = new TreeMap<String, Integer>();
            while (rs.next()){
                String author = rs.getString("author");
                int count = rs.getInt("count");
                map.put(author, count);
            }
            return map;
        }
    }

    private static final class MP3RowMapper implements RowMapper<MP3> {

        @Override
        public MP3 mapRow(ResultSet rs, int rowNum) throws SQLException {
            MP3 mp3 = new MP3();
            mp3.setId(rs.getInt("id"));
            mp3.setName(rs.getString("name"));
            mp3.setName(rs.getString("author"));
            return mp3;
        }
    }

}

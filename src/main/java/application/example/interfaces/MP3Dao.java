package application.example.interfaces;

import application.example.objects.Author;
import application.example.objects.MP3;

import java.util.List;
import java.util.Map;

public interface MP3Dao {

    int insertMp3 (MP3 mp3);

    void delete (int id);

    MP3 getMP3ById(int id);

    List<MP3> getMP3ListByName(String name);

    List<MP3> getMP3ListByAuthor(String author);

    Map<String, Integer> getStat();

    int insert2(MP3 mp3);

    public int insertAuthor(Author author);
}

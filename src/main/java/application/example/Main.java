package application.example;

import application.example.config.Config;
import application.example.impls.SQLiteDao;
import application.example.objects.Author;
import application.example.objects.MP3;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        MP3 mp3 = new MP3();
        mp3.setName("Song #55");

        Author author = new Author();
        author.setName("Author");

        mp3.setAuthor(author);

        ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        SQLiteDao sqliteDAO = (SQLiteDao) context.getBean("sqliteDAO");

        System.out.println(sqliteDAO.insertMp3(mp3));

    }
}
